#pragma once

#include "websvc_tree.h"

struct websvc_lex_extra
{
	::services *services = nullptr;

	~websvc_lex_extra() noexcept { delete services; }
};
