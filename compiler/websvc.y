%define api.pure full
%define parse.error verbose
%param {yyscan_t scanner}
%locations

%union {
	struct
	{
		char const *text;
		unsigned int leng;
		operator std::string() const
		{
			return std::string(text, leng);
		}
	} string;
	::simple_type simple_type;
	::direction direction;
	::encoding encoding;
	::endpoint_type endpoint_type;
	::method_selection method_selection;
	::type *type;
	::json_type_element *json_type_element;
	::json_map_elements *json_map_elements;
	::json_map_element *json_map_element;
	::services *services;
	::service *service;
	::service_elements *service_elements;
	::service_element *service_element;
	::scope_definitions *scope_definitions;
	::scope_definition *scope_definition;
	::endpoint_elements *endpoint_elements;
	::endpoint_element *endpoint_element;
	::action_elements *action_elements;
	::action_element *action_element;
}

%code requires {
#include "websvc_tree.h"
	typedef void *yyscan_t;
}

%code {
#include "websvc_lex.h"
#include "websvc_lex_extra.h"

#include <stdio.h>

void yyerror(YYLTYPE *yylval, yyscan_t scanner, char const *msg)
{
	FILE *log = yyget_out(scanner);
	websvc_lex_extra *extra = yyget_extra(scanner);

	fprintf(log, "%d:%d: %s\n", //%.*s\n",
			yylval->first_line, yylval->first_column + 1,
			msg); //,
			//extra->context.length(), extra->context.text());
}

}

%destructor { } <string> <direction> <encoding> <endpoint_type> <method_selection> <simple_type>
%destructor { delete $$; } <*>

%token end_of_file 0 "end of file"

%token ACTION "action"
%token ARRAY "array"
%token AUTHORIZATION "authorization"
%token BASE "base"
%token BOOLEAN "boolean"
%token DELETE "DELETE"
%token ENDPOINT "endpoint"
%token GET "GET"
%token HEADER "header"
%token IN "in"
%token INTEGER "integer"
%token JSON "json"
%token MAP "map"
%token METHOD "method"
%token OPAQUE "opaque"
%token OUT "out"
%token POST "POST"
%token REQUIRED "required"
%token REST "REST"
%token SCOPE "scope"
%token SERVICE "service"
%token TIMESTAMP "timestamp"
%token TYPE "type"
%token URLENCODED "urlencoded"

%token<string> IDENTIFIER
%token<string> STRING

%type<direction> direction
%type<encoding> named_encoding unnamed_encoding
%type<endpoint_type> endpoint_type
%type<simple_type> simple_type

%type<type> type json_type

%type<json_type_element> json_type_element json_map json_array json_simple
%type<json_map_elements> json_map_elements
%type<json_map_element> json_map_element

%type<services> services
%type<service> service
%type<service_elements> service_elements
%type<service_element>
	service_element
	default_endpoint_element
	base
	authorization_scopes
	endpoint

%type<scope_definitions> scope_definitions
%type<scope_definition> scope_definition

%type<endpoint_elements> endpoint_elements
%type<endpoint_element> endpoint_element action default_action_element

%type<action_elements> action_elements
%type<action_element> action_element parameter authorization_requirement type_definition authorization_method

%type<method_selection> method_selection

%%

start:
	services
		{ yyget_extra(scanner)->services = $1; }

services:
	services service
		{ $$ = new services { $1, $2 }; }
|	%empty
		{ $$ = nullptr; }

service:
	"service" STRING '{' service_elements '}'
		{ $$ = new service { $2, $4 }; }

service_elements:
	service_elements service_element
		{ $$ = new service_elements { $1, $2 }; }
|	%empty
		{ $$ = nullptr; }

service_element:
	default_endpoint_element
|	base
|	authorization_scopes
|	endpoint

authorization_scopes:
	"authorization" "scope" scope_definition
		{ $$ = new authorization_scopes(new scope_definitions { nullptr, $3 }); }
|	"authorization" "scope" '{' scope_definitions '}'
		{ $$ = new authorization_scopes($4); }

scope_definitions:
	scope_definitions scope_definition
		{ $$ = new scope_definitions { $1, $2 }; }
|	%empty
		{ $$ = nullptr; }

scope_definition:
	IDENTIFIER '=' STRING ';'
		{ $$ = new scope_definition { $1, $3 }; }

default_endpoint_element:
	default_action_element
		{ $$ = new default_endpoint_element($1); }

default_action_element:
	type_definition
		{ $$ = new default_action_element($1); }
|	authorization_method
		{ $$ = new default_action_element($1); }
|	parameter
		{ $$ = new default_action_element($1); }

type_definition:
	"type" IDENTIFIER ':' type ';'
		{ $$ = new type_definition($2, $4); }

type:
	simple_type
		{ $$ = new builtin_type($1); }
|	json_type
|	IDENTIFIER
		{ $$ = new type_reference($1); }

simple_type:
	"integer"
		{ $$ = TYPE_INTEGER; }
|	"opaque"
		{ $$ = TYPE_OPAQUE; }
|	"timestamp"
		{ $$ = TYPE_TIMESTAMP; }
|	"boolean"
		{ $$ = TYPE_BOOLEAN; }

json_type:
	"json" '{' json_type_element '}'
		{ $$ = new json_type($3); }
|	"json" json_type_element
		{ $$ = new json_type($2); }

json_type_element:
	json_map
|	json_array
|	json_simple

json_map:
	"map" '{' json_map_elements '}'
		{ $$ = new json_map($3); }

json_map_elements:
	json_map_elements json_map_element
		{ $$ = new json_map_elements { $1, $2 }; }
|	%empty
		{ $$ = nullptr; }

json_map_element:
	STRING ':' json_type_element ';'
		{ $$ = new json_map_element { $1, $3 }; }

json_array:
	"array" json_type_element
		{ $$ = new json_array($2); }
|	"array" '{' json_type_element '}'
		{ $$ = new json_array($3); }

json_simple:
	simple_type
		{ $$ = new json_simple_type($1); }

base:
	"base" STRING ';'
		{ $$ = new base($2); }

authorization_method:
	"authorization" "method" STRING ';'
		{ $$ = new authorization_method($3); }

parameter:
	direction named_encoding STRING ':' type ';'
		{ $$ = new parameter($1, $2, $3, $5); }
|	direction unnamed_encoding ':' type ';'
		{ $$ = new parameter($1, $2, $4); }

direction:
	"in"
		{ $$ = DIRECTION_IN; }
|	"out"
		{ $$ = DIRECTION_OUT; }

named_encoding:
	"header"
		{ $$ = ENCODING_HEADER; }
|	"urlencoded"
		{ $$ = ENCODING_URLENCODED; }

unnamed_encoding:
	"json"
		{ $$ = ENCODING_JSON; }

endpoint:
	"endpoint" STRING endpoint_type '{' endpoint_elements '}'
		{ $$ = new endpoint($2, $3, $5); }

endpoint_type:
	"REST"
		{ $$ = ENDPOINT_REST; }

endpoint_elements:
	endpoint_elements endpoint_element
		{ $$ = new endpoint_elements { $1, $2 }; }
|	%empty
		{ $$ = nullptr; }

endpoint_element:
	default_action_element
|	action

action:
	"action" STRING '{' action_elements '}'
		{ $$ = new action($2, $4); }

action_elements:
	action_elements action_element
		{ $$ = new action_elements { $1, $2 }; }
|	%empty
		{ $$ = nullptr; }

action_element:
	parameter
|	method_selection
		{ $$ = new action_method_selection($1); }
|	authorization_requirement

method_selection:
	"method" "GET" ';'
		{ $$ = METHOD_GET; }
|	"method" "POST" ';'
		{ $$ = METHOD_POST; }
|	"method" "DELETE" ';'
		{ $$ = METHOD_DELETE; }

authorization_requirement:
	"authorization" "required" IDENTIFIER ';'
		{ $$ = new authorization_requirement($3); }
