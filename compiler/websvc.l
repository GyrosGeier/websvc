%option 8bit
%option never-interactive
%option noyywrap
%option nounput
%option nodefault
%option bison-bridge
%option bison-locations
%option reentrant
%option warn
%option yylineno
%option extra-type="struct websvc_lex_extra *"

%{
#include "websvc_parse.h"
#include "websvc_lex_extra.h"
#include <stdio.h>

#define YY_USER_ACTION \
	{ \
		yylloc->first_line = yylloc->last_line; \
		yylloc->first_column = yylloc->last_column; \
		yylloc->last_line = yylineno; \
		yylloc->last_column = yycolumn + 1; \
		yycolumn += yyleng; \
		/*yyextra->context.add_text(yytext, yyleng); */ \
	}

%}

STRING		\"[^"]*\"
IDENTIFIER	[A-Za-z_][A-Za-z0-9_]*

%%

\ 		/* ignore */
\t		/* ignore */
\n		yycolumn = 0;

=		return '=';
:		return ':';
;		return ';';
\{		return '{';
\}		return '}';

DELETE		return DELETE;
GET		return GET;
POST		return POST;
REST		return REST;
action		return ACTION;
array		return ARRAY;
authorization	return AUTHORIZATION;
base		return BASE;
boolean		return BOOLEAN;
endpoint	return ENDPOINT;
header		return HEADER;
in		return IN;
integer		return INTEGER;
json		return JSON;
map		return MAP;
method		return METHOD;
opaque		return OPAQUE;
out		return OUT;
required	return REQUIRED;
scope		return SCOPE;
service		return SERVICE;
timestamp	return TIMESTAMP;
type		return TYPE;
urlencoded	return URLENCODED;

{STRING}	yylval->string.text = yytext; yylval->string.leng = yyleng; return STRING;
{IDENTIFIER}	yylval->string.text = yytext; yylval->string.leng = yyleng; return IDENTIFIER;

.		fprintf(stderr, "%u:%u: unhandled character %02x\n", yylineno, yycolumn, yytext[0]);
