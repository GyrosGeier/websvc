#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "websvc_parse.h"
#include "websvc_lex.h"
#include "websvc_lex_extra.h"

#include <iostream>

#include <cstdio>

int main(int argc, char **argv)
{
	for(char const *const *arg = argv + 1; arg != argv + argc; ++arg)
	{
		websvc_lex_extra context;

		yyscan_t scanner;
		yylex_init_extra(&context, &scanner);
		FILE *in = ::fopen(*arg, "r");
		if(!in)
		{
			std::cerr << "E: cannot open " << *arg << std::endl;
			return 1;
		}
		yyset_in(in, scanner);
		yyparse(scanner);
		yylex_destroy(scanner);
		::fclose(in);
	}
	return 0;
}
