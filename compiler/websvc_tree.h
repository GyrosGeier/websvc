#pragma once

#include <string>

#include <memory>

enum direction
{
	DIRECTION_IN,
	DIRECTION_OUT
};

enum encoding
{
	ENCODING_HEADER,
	ENCODING_URLENCODED,
	ENCODING_JSON
};

enum endpoint_type
{
	ENDPOINT_REST
};

enum method_selection
{
	METHOD_GET,
	METHOD_POST,
	METHOD_DELETE
};

enum simple_type
{
	TYPE_BOOLEAN,
	TYPE_INTEGER,
	TYPE_TIMESTAMP,
	TYPE_OPAQUE
};

struct json_type_element
{
	virtual ~json_type_element() noexcept { }
};

struct json_simple_type : json_type_element
{
	json_simple_type(simple_type kind)
	:
		kind(kind)
	{ }

	simple_type const kind;
};

struct json_array : json_type_element
{
	json_array(json_type_element *member_type)
	:
		member_type(member_type)
	{ }

	json_type_element *const member_type;

	~json_array() noexcept { delete member_type; }
};

struct json_map_element
{
	std::string const member_name;
	json_type_element *const member_type;

	~json_map_element() noexcept { delete member_type; }
};

struct json_map_elements
{
	json_map_elements *lhs = nullptr;
	json_map_element *rhs = nullptr;

	~json_map_elements() noexcept { delete lhs; delete rhs; }
};

struct json_map : json_type_element
{
	json_map(json_map_elements *elem)
	:
		elem(elem)
	{ }

	std::unique_ptr<json_map_elements> const elem;
};

struct type
{
	virtual ~type() noexcept { }
};

/* type referenced by an identifier name -- needs to be defined elsewhere
 */
struct type_reference : type
{
	type_reference(std::string const &name)
	:
		name(name)
	{ }

	std::string const name;
};

struct builtin_type : type
{
	builtin_type(simple_type kind)
	:
		kind(kind)
	{ }

	simple_type const kind;
};

/* connecting simple and JSON types */
struct json_type : type
{
	json_type(json_type_element *elem)
	:
		elem(elem)
	{ }

	std::unique_ptr<json_type_element> const elem;
};

struct action_element
{
	virtual ~action_element() noexcept { }
};

struct type_definition : action_element
{
	type_definition(
			std::string const &name,
			::type *type)
	:
		name(name),
		type(type)
	{ }

	std::string const name;
	std::unique_ptr<::type> const type;
};

struct parameter : action_element
{
	parameter(
			::direction direction,
			::encoding encoding,
			std::string const &name,
			::type *type)
	:
		direction(direction),
		encoding(encoding),
		name(name),
		type(type)
	{ }

	parameter(
			::direction direction,
			::encoding encoding,
			::type *type)
	:
		direction(direction),
		encoding(encoding),
		type(type)
	{ }

	::direction const direction;
	::encoding const encoding;
	std::string const name;
	std::unique_ptr<::type> const type;
};

struct authorization_method : action_element
{
	authorization_method(std::string const &method)
	:
		method(method)
	{ }

	std::string const method;
};

struct action_method_selection : action_element
{
	action_method_selection(::method_selection method)
	:
		method(method)
	{ }

	::method_selection const method;
};

struct authorization_requirement : action_element
{
	authorization_requirement(std::string const &req)
	:
		req(req)
	{ }

	std::string const req;
};

struct action_elements
{
	::action_elements *const lhs;
	::action_element *const rhs;

	~action_elements() noexcept { delete lhs; delete rhs; }
};

struct endpoint_element
{
	virtual ~endpoint_element() noexcept { }
};

struct action : endpoint_element
{
	action(
			std::string const &name,
			action_elements *def)
	:
		name(name),
		def(def)
	{ }

	std::string const name;
	std::unique_ptr<::action_elements> const def;
};

/* wraps an action element that appears as an endpoint element
 *
 * This is used when an action element is defined at endpoint scope,
 * and represents a default
 */
struct default_action_element : endpoint_element
{
	default_action_element(action_element *elem) : elem(elem) { }

	std::unique_ptr<action_element> const elem;
};

struct endpoint_elements
{
	::endpoint_elements *const lhs;
	::endpoint_element *const rhs;

	~endpoint_elements() noexcept { delete lhs; delete rhs; }
};

struct scope_definition
{
	std::string const id;
	std::string const name;
};

struct scope_definitions
{
	::scope_definitions *const lhs;
	::scope_definition *const rhs;

	~scope_definitions() noexcept { delete lhs; delete rhs; }
};

struct service_element
{
	virtual ~service_element() noexcept { }
};

struct default_endpoint_element : service_element
{
	default_endpoint_element(endpoint_element *elem) : elem(elem) { }

	std::unique_ptr<endpoint_element> const elem;
};

struct base : service_element
{
	base(std::string const &url)
	:
		url(url)
	{ }

	std::string const url;
};

struct authorization_scopes : service_element
{
	authorization_scopes(
			scope_definitions *def)
	:
		def(def)
	{ }

	std::unique_ptr<scope_definitions> const def;
};

struct endpoint : service_element
{
	endpoint(
			std::string const &name,
			::endpoint_type type,
			::endpoint_elements *elem)
	:
		name(name),
		type(type),
		elem(elem)
	{ }

	std::string const name;
	::endpoint_type const type;
	std::unique_ptr<::endpoint_elements> const elem;
};

struct service_elements
{
	::service_elements *lhs = nullptr;
	::service_element *rhs = nullptr;

	~service_elements() noexcept { delete lhs; delete rhs; }
};

struct service
{
	std::string name;
	::service_elements *parameters = nullptr;

	~service() noexcept { delete parameters; }
};

struct services
{
	::services *lhs = nullptr;
	::service *rhs = nullptr;

	~services() noexcept { delete lhs; delete rhs; }
};
